
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Use


INTRODUCTION
------------

Current Maintainer: Nitin Kaushik <imenitinkaushik90>

Pardot (A Salesforce product) is a lead generation system offering a SaaS based application. Pardot enables organizations to track and measure the effectiveness of their marketing campaigns. Pardot is integrated with a number of CRM tools, including Salesforce.

Pardot forms, is being used when to collect information about people visiting your website as a lead generation mechanism.

Using this module you will be able to embed the Pardot Form as a block and can be able to place any where on your website as per the requirement.


INSTALLATION
------------

Install as simple as other contrib module is installed in drupal.


USE
---

After installing the module, reach to "Block Layout" page.
1. Select "Place Block", for the region where you want to place the block.
2. Enter the Pardot Form URL. (Login to pardot applicatioon url and reach to "Pardot Forms" section and select any form among available. Here you will get "LINK")
3. Define the Width and Height of Iframe.
4. Set scrolling by checking the checkbox.
5. Save. 

That's it.
