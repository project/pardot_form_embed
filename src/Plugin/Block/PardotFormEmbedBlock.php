<?php
namespace Drupal\pardot_form_embed\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Block\BlockPluginInterface;
/**
 * Provides a 'PardotFormEmbed' block.
 *
 * @Block(
 *  id = "pardot_form_embed",
 *  admin_label = @Translation("Pardot Form Iframe Embed"),
 * )
 */
class PardotFormEmbedBlock extends BlockBase implements BlockPluginInterface {
  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();
    $form['pardot_form_url'] = array (
      '#type' => 'url',
      '#title' => $this->t('Pardot Form Link'),
      '#description' => $this->t('Visit your "Forms" page in pardot. Click on form you want to embed and then copy the "Link" value here.'),
      '#default_value' => isset($config['pardot_form_url']) ? $config['pardot_form_url'] : '',
      '#required' => TRUE,
    );
    $form['width'] = array (
      '#type' => 'number',
      '#title' => $this->t('Iframe Width'),
      '#description' => $this->t('Set the width of iframe here.'),
      '#default_value' => isset($config['width']) ? $config['width'] : 100,
      '#required' => TRUE,
      '#min' => 100,
      '#max' => 1500,
      '#field_suffix' => 'px',
    );
    $form['height'] = array (
      '#type' => 'number',
      '#title' => $this->t('Iframe Height'),
      '#description' => $this->t('Set the height of iframe here.'),
      '#default_value' => isset($config['height']) ? $config['height'] : 100,
      '#required' => TRUE,
      '#min' => 50,
      '#max' => 5000,
      '#field_suffix' => 'px',
    );
    $form['scrolling'] = array (
      '#type' => 'checkbox',
      '#title' => $this->t('Scrolling'),
      '#description' => $this->t('Set the scrolling for iframe either yes or no.'),
      '#default_value' => isset($config['scrolling']) ? $config['scrolling'] : 1,
    );
    
    return $form;
  }
  
  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    $url = $form_state->getValue('pardot_form_url');
    if (!filter_var($url, FILTER_VALIDATE_URL)){
      $form_state->setErrorByName('pardot_form_url', $this->t('This url is not a valid url'));
    }
  }
  
  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('pardot_form_url', $form_state->getValue('pardot_form_url'));
    $this->setConfigurationValue('width', $form_state->getValue('width'));
    $this->setConfigurationValue('height', $form_state->getValue('height'));
    $this->setConfigurationValue('scrolling', $form_state->getValue('scrolling'));
  }
  
  public function build() {
    $config = $this->getConfiguration();
    $data = [];
    $pardot_form_url = '';
    if (!empty($config['pardot_form_url'])) {
      $pardot_form_url = $config['pardot_form_url'] ?? '';
    }
    $data['pardot_form_url'] = $pardot_form_url;
    $data['width'] = $config['width'];
    $data['height'] = $config['height'];
    if($config['scrolling'] == 0){
      $data['scrolling'] = 'no';
    }else{
      $data['scrolling'] = 'yes';
    }
    return [
        '#theme' => 'pardot_form_embed',
        '#data' => $data,
        '#cache' => ['max-age' => 0],
    ];
  }
}
